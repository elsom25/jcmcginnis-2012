#!/usr/bin/env ruby
require 'optparse'	# option parser
require 'pathname'	# path and file analysis
require 'fileutils'	#
require 'open3'		# run command and get all output streams
require 'pp'		# pretty print (pp)
require 'yaml'		# read yaml file
require 'facets'	# awesome string manipulations

class Compiler

	def initialize(options = nil); setup(options) if options; end

	def compile(options = nil)
		setup(options) if options
		raise "You must provide the options." unless @options

		_ do puts "Begin project compilation:" end
		_ 2 do pp options end
		_ 1 do print_options end
		_ do puts end

		compile_compass if options[:compile].include? :compass
		compile_slim if options[:compile].include? :slim
		compile_coffee if options[:compile].include? :coffee
	end

	#--------------------------------------------------------------------------
	private

	#
	# initializes object
	def setup(options)
		@options = options

		# input
		@SRC_PATH 			= create_clean_path 'src'

		# output
		@OUTPUT_PATH		= create_clean_path 'app'
		@ASSET_OUTPUT_PATH 	= File.join(@OUTPUT_PATH, 'assets/')

		_ 1 do print_paths end
	end

	#
	# helper function for constants and other
	def create_clean_path(path)
		clean_path = Pathname.new(path).cleanpath
		_ 2 do puts "  -- create path:      #{clean_path}" end

		clean_path
	end

	def print_paths
		puts "Project base paths:"
		puts "  SRC_PATH:            #{@SRC_PATH}"
		puts "  OUTPUT_PATH:         #{@OUTPUT_PATH}"
		puts "  ASSET_OUTPUT_PATH:   #{@ASSET_OUTPUT_PATH}"
		puts 
	end

	#
	# do the block only if above specified verbosity level
	def _(level=0, &block)
		yield block if level <= @options[:verbosity]
	end

	def print_stdout(out)
		print_output(out, "standard output")
	end

	def print_stderr(err)
		print_output(err, "standard error", "*")
	end

	def print_output(output, type, separator=":")
		return unless output
		return if output.blank?

		puts "  == #{type.titlecase}"
		output.split("\n").each do |s|
			puts "    #{separator} #{s}" if s
		end
		puts
	end

	def print_options
		puts "  PRODUCTION:          #{@options[:production?]}"
		puts "  DEBUG:               #{@options[:debug?]}"
		puts "  VERBOSITY:           #{@options[:verbosity]}"
		puts "  COMPILE_LIST:        #{@options[:compile].join(', ')}"
	end

	#
	# Compile functions
	#

	#
	# Compile compass files using the default compass compiler
	def compile_compass
		_ do puts "Compiling Compass:" end

		compile_path = create_clean_path File.join(@SRC_PATH, 'compass/')
		config_path = create_clean_path File.join(compile_path, 'prod_config.rb') if @options[:production?]
		config_path = create_clean_path File.join(compile_path, 'debug_config.rb') if @options[:debug?]

		_ 1 do 
			puts "  SRC_PATH:            #{compile_path}"
			puts "  CONFIGURATION:       #{config_path}" if config_path
		end
		compile_compass_intern compile_path, config_path

		_ do puts end
	end

	def compile_compass_intern(path, config_path=nil)
		if config_path then
			out = run_command %Q[compass compile "#{path}" -c "#{config_path}" --force] 
		else
			out = run_command %Q[compass compile "#{path}"]
		end

		_ 1 do 
			print_stdout out[:stdout]
			print_stderr out[:stderr]
		end
	end

	#
	# Compile slim using the slim compiler
	def compile_slim
		_ do puts "Compiling Slim:" end

		_ do puts "  Compiling Slim Root:" end
		compile_slim_directory @SRC_PATH, @OUTPUT_PATH
		_ 2 do puts end

		_ do puts "  Compiling Slim Templates:" end
		template_path = create_clean_path File.join(@SRC_PATH, 'slim_tmpl/')
		template_output_path = create_clean_path File.join(@ASSET_OUTPUT_PATH, 'tmpl/')
		compile_slim_directory template_path, template_output_path

		_ do puts end
	end

	def compile_slim_file(input_base, output_path)
		# create output directory
		make_path_if_not_exists output_path

		# get data
		out = run_command %Q[slimrb -p "#{input_base}"]

		# save data
		input_file_name = File.basename(input_base, '.slim')
		output_file = File.join(output_path, input_file_name + '.html')
		File.open(output_file, 'w') { |f| f.write(out[:stdout]) }

		_ 2 do puts "    -- #{input_file_name}.slim => #{output_file}" end
		out
	end

	def compile_slim_directory(input_dir, output_path)
		raise "##ERR: input_dir (#{input_dir}) is not a directory." unless File.directory?(input_dir)

		_ 1 do 
			puts "    SRC_PATH:            #{input_dir}"
			puts "    OUTPUT_PATH:         #{output_path}"
			puts
		end			
		Dir.glob( File.join(input_dir, "*.slim") ) { |file|
			out = compile_slim_file file, output_path
			_ 1 do print_stderr out[:stderr] end
		}
	end

	#
	# Compile coffee using the standard coffee compiler.
	def compile_coffee
		_ do puts "Compiling Coffeescript:" end

		coffee_path = create_clean_path File.join(@SRC_PATH, 'coffee/')
		coffee_output_path = create_clean_path File.join(@ASSET_OUTPUT_PATH, "js/")

		_ 1 do 
			puts "    SRC_PATH:            #{coffee_path}"
			puts "    OUTPUT_PATH:         #{coffee_output_path}"
			puts
		end	

		# Build bootstrap for application
		bootstrap_file = create_clean_path File.join( coffee_path, 'bootstrap.coffee')

		_ do puts "  Compiling bootstrap file:" end
		_ 1 do 
			puts "    SRC_PATH:            #{bootstrap_file}"
			puts "    OUTPUT_PATH:         #{coffee_output_path}"
			puts
		end		
		out = run_command %Q[coffee --output "#{coffee_output_path}" --compile "#{bootstrap_file}"]

		_ 1 do 
			print_stdout out[:stdout]
			print_stderr out[:stderr]
		end

		# Build app for application
		app_file = create_clean_path File.join( coffee_path, 'app.coffee')

		_ do puts "  Compiling app file:" end
		_ 1 do 
			puts "    SRC_PATH:            #{app_file}"
			puts "    OUTPUT_PATH:         #{coffee_output_path}"
			puts
		end		
		out = run_command %Q[coffee --output "#{coffee_output_path}" --compile "#{app_file}"]

		_ 1 do 
			print_stdout out[:stdout]
			print_stderr out[:stderr]
		end

		_ do puts end
	end


	def run_command(cmd)
		_ 2 do puts "  exec #{cmd}" end
		stdin, stdout, stderr = Open3.popen3(cmd)		
		stdin.close

		out = { :stdout => stdout.read, :stderr => stderr.read }
		_ 2 do pp out end

		out
	end

	def make_path_if_not_exists(path)
		path = Pathname.new(path).cleanpath
		FileUtils.mkpath(path) unless File.exists?(path)
	end

end

#
# Option Parser

options = {
	:production? => false,
	:debug? => false,
	:verbosity => 0,
	:compile => [:compass, :slim, :coffee]
}

OptionParser.new do |opts|
	opts.banner = "Usage: example.rb [options]"

	opts.separator ""
	opts.separator "Specific options:"

	# PRODUCTION
	opts.on( '-p', '--production', "Concatenate and minify files for production" ) do
		options[:production?] = true
	end

	# DEBUG
	opts.on( '-d', '--debug', "Create files before minification steps" ) do
		options[:debug?] = true
	end

	# VERBOSITY
	opts.on( '-v', '--verbose N', Integer, "Print more execution details. 0 = standard output. 1 = detailed output. 2 = commands executed." ) do |v|
		v = 0 if v < 0
		options[:verbosity] = v
	end

	# MANUAL
	opts.on( '-c', '--compile [compass, slim, coffee]', Array,
			'Specify the types to compile' ) do |list|
		options[:compile] = list
		# @see http://stackoverflow.com/questions/800122/best-way-to-convert-strings-to-symbols-in-hash
		options[:compile] = options[:compile].collect{ |s| s.to_sym }
	end

	opts.separator ""
	opts.separator "Common options:"

	# HELP
	opts.on_tail( '-h', '--help', 'Show this message' ) do
		puts opts
		exit
	end

	# Another typical switch to print the version.
	opts.on_tail("--version", "Show version") do
		file = YAML.load_file('VERSION.yaml')
		puts "Project version information:"
		puts "  Version: #{file['version']}"
		puts "  Release: #{file['codename']}"
		exit
	end
end.parse!

#
# EXECUTE
#

Compiler.new.compile options