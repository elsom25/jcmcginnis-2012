require 'coffee-script'

desc "compile coffee-scripts from app/coffee to public/js"
task :coffee do
	source = "app/coffee/"
	javascripts = "public/js/"
	
	Dir.foreach(source) do |cf|
		unless cf == '.' || cf == '..' 
			js = CoffeeScript.compile File.read("#{source}#{cf}") 
			open "#{javascripts}#{cf.gsub('.coffee', '.js')}", 'w' do |f|
				f.puts js
			end 
		end 
	end
end