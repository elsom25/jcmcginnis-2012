require 'rubygems'
require 'sinatra/base'
require 'sinatra/partial'
require 'slim'

# =====================================
# Set Variables
# -------------------------------------

# Sinatra Base
set :app_file, __FILE__
set :root, File.dirname(__FILE__)
set :views, 'app'
set :public_folder, 'public'

# Sinatra Partial
set :partial_template_engine, :slim
#enable :partial_underscores

# Slim
set :slim, {:format => :html5}

# =====================================
# Application Routes
# -------------------------------------

APP_LAYOUT = :'layouts/app'

# home
['/', '/home/?'].each do |path|
	get path do
		slim :index, :layout => if request.xhr? then false else APP_LAYOUT end
	end
end

# work
get '/my-work/?' do
	slim :work, :layout => if request.xhr? then false else APP_LAYOUT end
end

# contact
get '/contact/?' do
	slim :contact, :layout => if request.xhr? then false else APP_LAYOUT end
end

get '/resume/?' do
	send_file File.join( settings.public_folder, 'resume.html' )
end

# 404
#not_found do
#  slim :404, :layout => APP_LAYOUT
#end
