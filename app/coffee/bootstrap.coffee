# load files in parallel but execute them in sequence
head.js(

  # external library files
  { console    : "/js/lib/console.min.js"         }  # safe logging everywhere!
  { jquery     : "/js/lib/jquery.js"              }
  { foundation : "/js/lib/foundation.min.js"      }
  { squaresend : "//squaresend.com/squaresend.js" }

  # root application file
  { app        : "/js/app.js" }

)

head.ready( "app", ->

  debug.log "app loaded"

)
