jQuery ($) ->
	$doc = $( document )

	# 
	# Foundation

	if $.fn.foundationAlerts           then $doc.foundationAlerts() else null
	if $.fn.foundationAccordion        then $doc.foundationAccordion() else null
	if $.fn.foundationTooltips         then $doc.foundationTooltips() else null
	if $.fn.foundationButtons          then $doc.foundationButtons() else null
	if $.fn.foundationNavigation       then $doc.foundationNavigation() else null
	if $.fn.foundationTopBar           then $doc.foundationTopBar( breakPoint: 480 ) else null
	if $.fn.foundationMediaQueryViewer then $doc.foundationMediaQueryViewer() else null
	if $.fn.foundationTabs             then $doc.foundationTabs() else null
	$( 'input, textarea' ).placeholder()
	$( '#featured' ).orbit()

	# hide address bar on mobile devices
	Modernizr = window.Modernizr
	if Modernizr.touch
		$( window ).load( () ->
			setTimeout( () -> 
				window.scrollTo( 0, 1 )
			, 0)
		)

	#
	# AJAX Navigation

	changeContent = (e) ->
		e.preventDefault()
		e.stopImmediatePropagation()

		url = $( e.target ).attr( 'href' )
		$( '#main' ).load( url )

		data =
			state: $( e.target ).attr( 'title' ),
			url: url

		window.history.pushState( data, 'Title', url )

		return false

	$( '#main-nav' ).delegate( 'a', 'click', changeContent )
	$( '#footer-nav' ).delegate( 'a', 'click', changeContent )
	$( window ).on( 'popstate', (event) ->
		state = event.originalEvent.state

		if state
			$( 'title' ).text( state.state )
			$( '#main' ).load( state.url )
	)
