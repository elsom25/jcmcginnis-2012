(function() {

  head.js({
    console: "/js/lib/console.min.js"
  }, {
    jquery: "/js/lib/jquery.js"
  }, {
    foundation: "/js/lib/foundation.min.js"
  }, {
    app: "/js/app.js"
  });

  head.ready("app", function() {
    return debug.log("app loaded");
  });

}).call(this);
