(function() {

  jQuery(function($) {
    var $doc, Modernizr, changeContent;
    $doc = $(document);
    if ($.fn.foundationAlerts) {
      $doc.foundationAlerts();
    } else {
      null;
    }
    if ($.fn.foundationAccordion) {
      $doc.foundationAccordion();
    } else {
      null;
    }
    if ($.fn.foundationTooltips) {
      $doc.foundationTooltips();
    } else {
      null;
    }
    if ($.fn.foundationButtons) {
      $doc.foundationButtons();
    } else {
      null;
    }
    if ($.fn.foundationNavigation) {
      $doc.foundationNavigation();
    } else {
      null;
    }
    if ($.fn.foundationTopBar) {
      $doc.foundationTopBar({
        breakPoint: 480
      });
    } else {
      null;
    }
    if ($.fn.foundationMediaQueryViewer) {
      $doc.foundationMediaQueryViewer();
    } else {
      null;
    }
    if ($.fn.foundationTabs) {
      $doc.foundationTabs();
    } else {
      null;
    }
    $('input, textarea').placeholder();
    $('#featured').orbit();
    Modernizr = window.Modernizr;
    if (Modernizr.touch) {
      $(window).load(function() {
        return setTimeout(function() {
          return window.scrollTo(0, 1);
        }, 0);
      });
    }
    changeContent = function(e) {
      var data, url;
      e.preventDefault();
      e.stopImmediatePropagation();
      url = $(e.target).attr('href');
      $('#main').load(url);
      data = {
        state: $(e.target).attr('title'),
        url: url
      };
      window.history.pushState(data, 'Title', url);
      return false;
    };
    $('#main-nav').delegate('a', 'click', changeContent);
    $('#footer-nav').delegate('a', 'click', changeContent);
    return $(window).on('popstate', function(event) {
      var state;
      state = event.originalEvent.state;
      if (state) {
        $('title').text(state.state);
        return $('#main').load(state.url);
      }
    });
  });

}).call(this);
